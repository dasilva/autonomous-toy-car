# Project Description

Our aim is to start building a self-driving car functional pipeline, using deep machine learning methods. 

# Opencv installation
pip install opencv-pyhton

# Tensorflow installation
pip install tensorflow==1.15

## Lane Detection 
To visualize lane detection using OpenCV, go to lane_detection/Lane_Detection.ipynb

## HOG feature + SVM classifier for traffic sign detection 
Run the notebook MDS_UL_traffic_sign_detection_SVM+Hog.ipynb

## SSD+Mobilenet classifier for traffic sign detection
Run the notebook MDS_UL_traffic_sign_detection_Mobilenet+SSD.ipynb
